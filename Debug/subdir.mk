################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../BIPTechBTModule.cpp \
../BIPTechBluetoothModule.cpp \
../BIPTechErrorHandler.cpp \
../BIPTechNotification.cpp \
../BTLEModule.cpp \
../BTSensorTagModule.cpp \
../DynamicModule.cpp 

C_SRCS += \
../utilities.c 

OBJS += \
./BIPTechBTModule.o \
./BIPTechBluetoothModule.o \
./BIPTechErrorHandler.o \
./BIPTechNotification.o \
./BTLEModule.o \
./BTSensorTagModule.o \
./DynamicModule.o \
./utilities.o 

C_DEPS += \
./utilities.d 

CPP_DEPS += \
./BIPTechBTModule.d \
./BIPTechBluetoothModule.d \
./BIPTechErrorHandler.d \
./BIPTechNotification.d \
./BTLEModule.d \
./BTSensorTagModule.d \
./DynamicModule.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	arm-linux-gnueabihf-g++ -D__CAMERA_MODULE__ -I/home/marc/rpi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bluez-5.4 -I/home/marc/Documents/poco-1.4.7/Util/include -I/home/marc/workspace/common -I/home/marc/workspace/libraries/src -I/home/marc/workspace/libraries/wiringPi/wiringPi -I/home/marc/Documents/poco-1.4.7/Foundation/include -I/home/marc/Documents/poco-1.4.7/Net/include -I/home/marc/workspace -I/home/marc/workspace/libraries/src/cJSON -I/usr/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	arm-linux-gnueabihf-gcc -I/home/marc/workspace/libraries/src/cJSON -I/home/marc/workspace/libraries/src -I/home/marc/workspace/common -I/home/marc/Documents/poco-1.4.7/Util/include -I/home/marc/Documents/poco-1.4.7/Net/include -I/home/marc/Documents/poco-1.4.7/Foundation/include -I/home/marc/workspace/libraries/wiringPi/wiringPi -I/home/marc/workspace -I/usr/include -I/home/marc/rpi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bluez-5.4 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


