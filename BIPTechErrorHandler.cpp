/*
 * BIPTechErrorHandler.cpp
 *
 *  Created on: Nov 10, 2014
 *      Author: root
 */

#include <BIPTech/BIPTechErrorHandler.h>
#include <iostream>


namespace BIPTech {

BIPTechErrorHandler::BIPTechErrorHandler() {
	// TODO Auto-generated constructor stub

}

BIPTechErrorHandler::~BIPTechErrorHandler() {
	// TODO Auto-generated destructor stub
}

void BIPTechErrorHandler::exception(const Poco::Exception& exc)
{
	std::cerr << exc.displayText() << std::endl;
	std::cout << exc.displayText() << std::endl;
}

void BIPTechErrorHandler::exception(const std::exception& exc)
{
	std::cerr << exc.what() << std::endl;
	std::cout << exc.what() << std::endl;
}

void BIPTechErrorHandler::exception()
{
	std::cerr << "unknown exception" << std::endl;
	std::cout << "unknown exception" << std::endl;
}


} /* namespace BIPTech */
