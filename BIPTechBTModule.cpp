/*
 * BIPTechBTModule.cpp
 *
 *  Created on: Nov 26, 2014
 *      Author: root
 */

#include "BIPTechInterfaces.h"
#include "BIPTechBTModule/BIPTechBTModule.h"
#include "BIPTechBTModule/BIPTechNotification.h"
#include <BIPTechInterfaces.h>
#include <Poco/Notification.h>

using Poco::Notification;

namespace BIPTechBluetooth {

stDataTypeMask btDataTypes[] =
{
		{ "READ", "", "TEMP", "BT", 0, "BTNotification", 0x0000000000000001},
		{ "READ", "", "ACCEL", "BT", 0, "BTNotification", 0x0000000000000002}
};


BIPTechBTModule::BIPTechBTModule()
{
	unsigned int i;
	for(i=0;i<sizeof(btDataTypes); i++)
	{
		_lstDataTypeMask.push_back(&(btDataTypes[i]));
	}
}

BIPTechBTModule::~BIPTechBTModule()
{

}


std::list<pDataTypeMask>::iterator BIPTechBTModule::getDataTypeMaskBeginItor()
{
	return _lstDataTypeMask.begin();
}

std::list<pDataTypeMask>::iterator BIPTechBTModule::getDataTypeMaskEndItor()
{
	return _lstDataTypeMask.end();
}

void BIPTechBTModule::start()
{

}

void BIPTechBTModule::stop()
{

}

void BIPTechBTModule::pause()
{

}

void BIPTechBTModule::Notify(Notification::Ptr pNotification)
{
	this->NotifyWithPriority(pNotification, 10);
}

void BIPTechBTModule::NotifyWithPriority(Notification::Ptr pNotification, int nPriority)
{
	if( this->usesPriorityQueue() ) {
		this->getPriorityNotifyQueue()->enqueueNotification((Poco::Notification*)pNotification, nPriority);
	} else {
		this->getNotifyQueue()->enqueueNotification((Notification::Ptr)pNotification);
	}
}

const IBIPTechNotificationFactory* BIPTechBTModule::getModuleNotificationFactory()
{
	return NULL;
}

}

