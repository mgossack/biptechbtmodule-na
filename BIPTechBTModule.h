/*
 * BIPTechBTModule.h
 *
 *  Created on: Nov 26, 2014
 *      Author: root
 */

#ifndef BIPTECHBTMODULE_H_
#define BIPTECHBTMODULE_H_

#include <BIPTechInterfaces.h>
#include "BIPTechBTModule/BIPTechNotification.h"
#include <stdio.h>
#include <string.h>
#include <list>
#include <Poco/Notification.h>
#include <Poco/NotificationQueue.h>
#include <Poco/PriorityNotificationQueue.h>


using Poco::Notification;
using Poco::NotificationQueue;
using Poco::PriorityNotificationQueue;


namespace BIPTechBluetooth {

class BIPTechBTModule: public IBIPTechModule
{
private:
	std::list<pDataTypeMask>	_lstDataTypeMask;

	PriorityNotificationQueue		_notifyQueue;

public:
	BIPTechBTModule();
	virtual ~BIPTechBTModule();

	// IBIPTechModule
	std::list<pDataTypeMask>::iterator getDataTypeMaskBeginItor();
	std::list<pDataTypeMask>::iterator getDataTypeMaskEndItor();
	void start();
	void stop();
	void pause();

	bool usesPriorityQueue() { return true; }
	Poco::NotificationQueue* getNotifyQueue() { return NULL; }
	Poco::PriorityNotificationQueue* getPriorityNotifyQueue() { return &_notifyQueue; }

	void Notify(Notification::Ptr notification);
	void NotifyWithPriority(Notification::Ptr notification, int nPriority);

	const IBIPTechNotificationFactory* getModuleNotificationFactory();
};

}

#endif
