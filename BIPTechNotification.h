/*
 * BIPTechNotification.h
 *
 *  Created on: Nov 28, 2014
 *      Author: root
 */

#ifndef BIPTECHNOTIFICATION_H_
#define BIPTECHNOTIFICATION_H_

#include <BIPTechInterfaces.h>
#include <Poco/Notification.h>
#include <Poco/AutoPtr.h>
#include "cmdstructs.h"
#include <map>
#include <list>

using Poco::AutoPtr;
using Poco::Notification;

namespace BIPTechBluetooth {


class BIPTechNotification : public Poco::Notification
{
	ClientCmd	_clientCmd;

public:
	BIPTechNotification() {}

	void setClientCmd(STCLIENTCMD& cmd) { _clientCmd = cmd; }
	STCLIENTCMD& getClientCmd() { return _clientCmd; }

	static Notification::Ptr Create(void) { return new BIPTechNotification(); };

	virtual std::string getTypename() { return "BIPTechNotification"; }
};

class BaseNotification : public BIPTechNotification
{
public:
	BaseNotification() {};

	static Notification::Ptr Create(void) { return new BaseNotification(); };

	virtual std::string getTypename() { return "BaseNotification"; }
};


class SubNotification : public BIPTechNotification
{
public:
	SubNotification() {};

	static Notification::Ptr Create(void) { return new SubNotification(); };

	virtual std::string getTypename() { return "SubNotification"; }
};

typedef Notification::Ptr ( *CreateNotificationFn)(void);


class BluetoothNotificationFactory : public IBIPTechNotificationFactory
{
private:
	typedef std::map<std::string, CreateNotificationFn> FactoryMap;
	FactoryMap _factoryMap;

public:
	void registerNotification(std::string notificationName, CreateNotificationFn pfnCreate );
	BluetoothNotificationFactory() {};
	virtual ~BluetoothNotificationFactory() {};

	Notification::Ptr createNotification(std::string notificationName);
};



} // namespace BIPTechBluetooth

#endif /* BIPTECHNOTIFICATION_H_ */
