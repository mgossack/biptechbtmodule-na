/*
 * BIPTechBluetoothModule.cpp
 *
 *  Created on: Nov 26, 2014
 *      Author: root
 */

#include "BIPTechInterfaces.h""
#include <BIPTechBTModule/BIPTechBluetoothModule.h>
#include "Poco/Message.h"
#include "Poco/Logger.h"

typedef void (*setScanFunc)(bool scan);
typedef void (*setHCIFunc)(int hci);

POCO_BEGIN_MANIFEST(IBIPTechModulePlugin)
	POCO_EXPORT_CLASS(BIPTechBluetoothModule)
POCO_END_MANIFEST

// optional set up and clean up functions
void pocoInitializeLibrary()
{
	std::cout << "PluginLibrary initializing" << std::endl;
}
void pocoUninitializeLibrary()
{
	std::cout << "PluginLibrary uninitializing" << std::endl;
}

//namespace BIPTechBluetooth {

BIPTechBluetoothModule::BIPTechBluetoothModule()
{
	// TODO Auto-generated constructor stub

}

BIPTechBluetoothModule::~BIPTechBluetoothModule() {
	// TODO Auto-generated destructor stub
}

void BIPTechBluetoothModule::load(std::string path, std::string name)
{
	DynamicModule::load(path, name);

	if( sharedLib.hasSymbol("setScan") ) {
		_setScan = (setScanFunc) sharedLib.getSymbol("setScan");
	} else {
//		poco_error(BIPSystem::getLogger(), "BTLEModule:" + getPath() + " does not have symbol 'setScan'");
		exit(1);
	}
	if( sharedLib.hasSymbol("setHCI") ) {
		_setHCI = (setHCIFunc) sharedLib.getSymbol("setHCI");
	} else {
//		poco_error(BIPSystem::getLogger(), "BTLEModule:" + getPath() + " does not have symbol 'setHCI'");
		exit(1);
	}
}

void BIPTechBluetoothModule::unload()
{
	DynamicModule::unload();
}

// Number of BIPTech modules in this loadable file
int BIPTechBluetoothModule::moduleTypeCount()
{
	return 1;
}

int BIPTechBluetoothModule::moduleCountOfType(int nModuleTypeNum )
{
	// return the number of object in existence for the given type
	if( nModuleTypeNum == 0 ) {
		return -1;
	}

	return -1;
}

int BIPTechBluetoothModule::createableMaximumOfType(int nModuleTypeNum)
{
	if( nModuleTypeNum == 0 )
	{
		// Determine the number of Bluetooth LE physical devices we have to work with
		// Verify there are no restrictions
	}
	return -99;	// Error?
}


IBIPTechModule* BIPTechBluetoothModule::createBIPTechModule(int nModuleTypeNum)
{
	// Verify that we haven't created the maximum number of modules yet.
	// Create a new one if possible and return pointer.
	return NULL;
}

IBIPTechModule* BIPTechBluetoothModule::getBIPTechModule(int nModuleTypeNum, int nModuleNum)
{
	//
	return NULL;
}


IBIPTechNotificationFactory* BIPTechBluetoothModule::getPluginNotificationFactory()
{
	return NULL;
}

//} // namespace BIPTechBluetooth
