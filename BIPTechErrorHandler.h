/*
 * BIPTechErrorHandler.h
 *
 *  Created on: Nov 10, 2014
 *      Author: root
 */

#ifndef BIPTECHERRORHANDLER_H_
#define BIPTECHERRORHANDLER_H_

#include "Poco/ErrorHandler.h"


namespace BIPTech {

class BIPTechErrorHandler : public Poco::ErrorHandler {
public:
	BIPTechErrorHandler();
	virtual ~BIPTechErrorHandler();

	void exception(const Poco::Exception& exc);
	void exception(const std::exception& exc);
	void exception();

};

}; /* namespace BIPTech */


#endif /* BIPTECHERRORHANDLER_H_ */
