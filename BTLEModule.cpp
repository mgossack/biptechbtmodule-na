/*
 * BTLEModule.cpp
 *
 *  Created on: Nov 20, 2014
 *      Author: root
 */


#include "BIPTechInterfaces.h"
#include <BIPTechBTModule/BTLEModule.h>
#include <BIPTechBTModule/BIPTechNotification.h>
#include <Poco/Logger.h>
#include <Poco/RunnableAdapter.h>
#include <stdlib.h>

using namespace std;

using Poco::RunnableAdapter;
using BIPTechBluetooth::BaseNotification;
using BIPTechBluetooth::BIPTechNotification;



stDataTypeMask btleDataTypes[] =
{
		{ "READ", "", "", "BTLE", 0, "BTNotification", 0x0000000000000001},
		{ "WRITE", "", "", "BTLE", 0, "BTNotification", 0x0000000000000002}
};

namespace BIPTechBluetooth {

BTLEModule::BTLEModule()
{
	unsigned int i;
	for(i=0;i<sizeof(btleDataTypes); i++)
	{
		_lstDataTypeMask.push_back(&(btleDataTypes[i]));
	}

	pRunnable = new Poco::RunnableAdapter<BTLEModule>(*this, &BTLEModule::run);

	_notificationFactory.registerNotification("BTNotification", &BaseNotification::Create );

}

BTLEModule::~BTLEModule()
{
}


std::list<pDataTypeMask>::iterator BTLEModule::getDataTypeMaskBeginItor()
{
	return _lstDataTypeMask.begin();
}

std::list<pDataTypeMask>::iterator BTLEModule::getDataTypeMaskEndItor()
{
	return _lstDataTypeMask.end();
}

void BTLEModule::start()
{
	_thread.start(*pRunnable);
}

void BTLEModule::stop()
{

}

void BTLEModule::pause()
{

}

void BTLEModule::run()
{
	BIPTechNotification	*pNotification;

	while( _thread.isRunning() ) {
		if( this->usesPriorityQueue() ) {
			pNotification = (BIPTechNotification*)this->getPriorityNotifyQueue()->waitDequeueNotification();
		} else {
			pNotification = (BIPTechNotification*)this->getNotifyQueue()->waitDequeueNotification();
		}

		if(pNotification) {
			// Process the command
			// pNotification->clientCmd()...
		}
	}
}

void BTLEModule::Notify(BIPTechNotification *pNotification)
{
	if( this->usesPriorityQueue() ) {
		this->NotifyWithPriority(pNotification, 10);
	} else {
		this->getNotifyQueue()->enqueueNotification(pNotification);
	}
}

void BTLEModule::NotifyWithPriority(BIPTechNotification *pNotification, int nPriority)
{
	if( this->usesPriorityQueue() ) {
		this->getPriorityNotifyQueue()->enqueueNotification(pNotification, nPriority);
	} else {
		this->getNotifyQueue()->enqueueNotification(pNotification);
	}
}

const IBIPTechNotificationFactory* BTLEModule::getModuleNotificationFactory()
{
	return &_notificationFactory;
}

} // namespace BIPTechBluetooth

