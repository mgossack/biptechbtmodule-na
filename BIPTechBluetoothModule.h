/*
 * BIPTechBluetoothModule.h
 *
 *  Created on: Nov 26, 2014
 *      Author: root
 */

#ifndef BIPTECHBLUETOOTHMODULE_H_
#define BIPTECHBLUETOOTHMODULE_H_

#include <BIPTechInterfaces.h>
#include <Poco/ClassLibrary.h>
#include <BIPTechBTModule/DynamicModule.h>



typedef void (*setScanFunc)(bool scan);
typedef void (*setHCIFunc)(int hci);



//namespace BIPTechBluetooth {

class BIPTechBluetoothModule: public DynamicModule {
private:
	std::string		_path;
	std::string		_name;
//	std::string		_ip;
//	int				_port;

	bool	_scan;		// true or false
	int		_hci;		// HCI #

	setScanFunc _setScan;
	setHCIFunc	_setHCI;

protected:
//	// Library

	virtual void load(std::string path, std::string name);
	virtual void unload();

public:
	BIPTechBluetoothModule();

	BIPTechBluetoothModule(std::string path, std::string name);
	~BIPTechBluetoothModule();

	int moduleTypeCount();	// Number of BIPTech modules in this loadable file
	int moduleCountOfType(int nModuleTypeNum );	// Return the number of objects of the module type in existence
	int createableMaximumOfType(int nModuleTypeNum);	// returns the maximum number of creatable objects of this type
	IBIPTechModule* createBIPTechModule(int nModuleTypeNum);
	IBIPTechModule* getBIPTechModule(int nModuleTypeNum, int nModuleNum);
	IBIPTechNotificationFactory* getPluginNotificationFactory();

};

//} // namespace BIPTechBluetooth

#endif /* BIPTECHBLUETOOTHMODULE_H_ */
