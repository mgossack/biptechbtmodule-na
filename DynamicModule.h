/*
 * DynamicModule.h
 *
 *  Created on: Nov 20, 2014
 *      Author: root
 */

#ifndef DYNAMICMODULE_H_
#define DYNAMICMODULE_H_

#include <Poco/SharedLibrary.h>
#include <BIPTechInterfaces.h>


typedef void (*setNameFunc)(std::string name);
typedef void (*setPortFunc)(int port);
typedef int (*getIPFunc)();

//namespace BIPTechBluetooth {

class DynamicModule : public IBIPTechModulePlugin {
private:
	std::string		_path;
	std::string		_name;
//	std::string		_ip;		// Specialize
//	int				_port;

protected:
	// Library
	Poco::SharedLibrary		sharedLib;


	virtual void load(std::string path, std::string name);
	virtual void unload();

private:

	// entry points into loaded library module
	setNameFunc _setName;
//	setPortFunc _setPort;
//	getIPFunc _getIP;

public:
	DynamicModule();
	DynamicModule(std::string path);
	virtual ~DynamicModule();

	std::string	getPath() { return _path; };
	std::string getName() { return _name; };
//	std::string getIP() { _ip = _getIP(); return _ip; };
//	int			getPort() { return _port; };

	void setPath(std::string path) { _path = path; }
	void setName(std::string name) { _name = name; _setName(_name); }
//	void setPort(int port) { _port = port; _setPort(_port); };

public:
	virtual int moduleTypeCount() = 0;	// Number of BIPTech modules in this loadable file
	virtual int moduleCountOfType(int nModuleTypeNum ) = 0;	// Return the number of objects of the module type in existence
	virtual int createableMaximumOfType(int nModuleTypeNum) = 0;	// returns the maximum number of creatable objects of this type
	virtual IBIPTechModule* createBIPTechModule(int nModuleTypeNum) = 0;
	virtual IBIPTechModule* getBIPTechModule(int nModuleTypeNum, int nModuleNum) = 0;

	virtual IBIPTechNotificationFactory* getPluginNotificationFactory() = 0;
};

//} // namespace BIPTechBluetooth

#endif /* DYNAMICMODULE_H_ */
