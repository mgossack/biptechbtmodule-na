################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../BIPHTTPRequestHandlerFactory.cpp \
../BIPSystem.cpp \
../BIPTechErrorHandler.cpp \
../BIPTechHTTPServer.cpp \
../BTLEModule.cpp \
../CameraModule.cpp \
../ClientServiceHandler.cpp \
../CmdQueueHandler.cpp \
../DataHandler.cpp \
../DataTypeMaskHandler.cpp \
../DynamicModule.cpp \
../IClientConnectionDataQueue.cpp \
../RootHandler.cpp \
../ServerConnector.cpp \
../TCPIncomingConnection.cpp \
../TestClass.cpp \
../UDPBroadcastHandler.cpp \
../main.cpp 

C_SRCS += \
../utilities.c 

OBJS += \
./BIPHTTPRequestHandlerFactory.o \
./BIPSystem.o \
./BIPTechErrorHandler.o \
./BIPTechHTTPServer.o \
./BTLEModule.o \
./CameraModule.o \
./ClientServiceHandler.o \
./CmdQueueHandler.o \
./DataHandler.o \
./DataTypeMaskHandler.o \
./DynamicModule.o \
./IClientConnectionDataQueue.o \
./RootHandler.o \
./ServerConnector.o \
./TCPIncomingConnection.o \
./TestClass.o \
./UDPBroadcastHandler.o \
./main.o \
./utilities.o 

C_DEPS += \
./utilities.d 

CPP_DEPS += \
./BIPHTTPRequestHandlerFactory.d \
./BIPSystem.d \
./BIPTechErrorHandler.d \
./BIPTechHTTPServer.d \
./BTLEModule.d \
./CameraModule.d \
./ClientServiceHandler.d \
./CmdQueueHandler.d \
./DataHandler.d \
./DataTypeMaskHandler.d \
./DynamicModule.d \
./IClientConnectionDataQueue.d \
./RootHandler.d \
./ServerConnector.d \
./TCPIncomingConnection.d \
./TestClass.d \
./UDPBroadcastHandler.d \
./main.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	arm-linux-gnueabihf-g++ -D__CAMERA_MODULE__ -I/home/marc/rpi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bluez-5.4 -I/home/marc/Documents/poco-1.4.7/Util/include -I/home/marc/workspace/common -I/home/marc/workspace/libraries/src -I/home/marc/workspace/libraries/wiringPi/wiringPi -I/home/marc/Documents/poco-1.4.7/Foundation/include -I/home/marc/Documents/poco-1.4.7/Net/include -I/home/marc/workspace -I/home/marc/workspace/libraries/src/cJSON -I/usr/include -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	arm-linux-gnueabihf-gcc -D__CAMERA_MODULE__ -I/home/marc/workspace/libraries/src/cJSON -I/home/marc/workspace/libraries/src -I/home/marc/workspace/common -I/home/marc/Documents/poco-1.4.7/Util/include -I/home/marc/Documents/poco-1.4.7/Net/include -I/home/marc/Documents/poco-1.4.7/Foundation/include -I/home/marc/workspace/libraries/wiringPi/wiringPi -I/home/marc/workspace -I/usr/include -I/home/marc/rpi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bluez-5.4 -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


