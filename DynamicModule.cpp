/*
 * DynamicModule.cpp
 *
 *  Created on: Nov 20, 2014
 *      Author: root
 */

#include <BIPTechBTModule/DynamicModule.h>
#include <Poco/Message.h>
#include <Poco/Logger.h>
#include <BIPTech/BIPSystem.h>

using namespace std;
using Poco::SharedLibrary;
using Poco::Logger;

//namespace BIPTechBluetooth {

DynamicModule::DynamicModule() {
	// TODO Auto-generated constructor stub
	// Protected
}


DynamicModule::DynamicModule(std::string path):
	_path(path)
{
	load(_path, std::string(""));
}

DynamicModule::~DynamicModule() {
	unload();
}

void DynamicModule::load(std::string path, std::string name) {
	// Load the module
	sharedLib.load(_path);

	if( sharedLib.hasSymbol("setName") ) {
		_setName = (setNameFunc) sharedLib.getSymbol("setName");
		_setName(name);
	} else {
		poco_error(BIPSystem::getLogger(), "DynamicModule:" + _path + " does not have symbol 'setName'");
		exit(1);
	}
//	if( sharedLib.hasSymbol("setPort") ) {
//		_setPort = (setPortFunc) sharedLib.getSymbol("setPort");
//	} else {
//		poco_error(BIPSystem::getLogger(), "DynamicModule:" + _path + " does not have symbol 'setPort'");
//		exit(1);
//	}
//
//	if( sharedLib.hasSymbol("getIP") ) {
//		_getIP = (getIPFunc) sharedLib.getSymbol("getIP");
//	} else {
//		poco_error(BIPSystem::getLogger(), "DynamicModule:" + _path + " does not have symbol 'getIP'");
//		exit(1);
//	}
}

void DynamicModule::unload() {
	sharedLib.unload();
}

//} // namespace BIPTechBluetooth
