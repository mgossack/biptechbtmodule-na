/*
 * BIPTechNotification.cpp
 *
 *  Created on: Nov 28, 2014
 *      Author: root
 */

#include <BIPTechBTModule/BIPTechNotification.h>


namespace BIPTechBluetooth {


void BluetoothNotificationFactory::registerNotification(std::string notificationName, CreateNotificationFn pfnCreate )
{
	_factoryMap[notificationName] = pfnCreate;
}

Notification::Ptr BluetoothNotificationFactory::createNotification(std::string notificationName)
{
	FactoryMap::iterator it = _factoryMap.find(notificationName);
	if( it != _factoryMap.end() )
		return it->second();

	return NULL;
}


} /* namespace BIPTechBluetooth */
