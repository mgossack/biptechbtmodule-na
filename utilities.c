/*
 * utilities.c
 *
 *  Created on: Aug 8, 2014
 *      Author: root
 */

#include "utilities.h"
#include <stdbool.h>


void WriteFrmtd(char *format, ...)
{
   va_list args;
   va_start(args, format);
   vprintf(format, args);
   va_end(args);
}


/**
 * returns dotted quad address of this machine.
 * Will attempt to use the hardwire first, then the wlan next
 */
char *getIPAddress(char *buf, char *eth, char *wlan)
{
    int sock, i;
    struct ifreq ifreqs[20];
    struct ifconf ic;

    ic.ifc_len = sizeof ifreqs;
    ic.ifc_req = ifreqs;

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        perror("socket");
        exit(1);
    }

    if (ioctl(sock, SIOCGIFCONF, &ic) < 0) {
        perror("SIOCGIFCONF");
        exit(1);
    }

    for (i = 0; i < ic.ifc_len/sizeof(struct ifreq); ++i)
    {
    	if( strncmp("eth", ifreqs[i].ifr_name, 3) == 0) {
        	strcpy(eth, inet_ntoa(((struct sockaddr_in*)&ifreqs[i].ifr_addr)->sin_addr));
    	}
    	else if(strncmp("wlan", ifreqs[i].ifr_name, 4) == 0)
    	{
    		strcpy( wlan, inet_ntoa(((struct sockaddr_in*)&ifreqs[i].ifr_addr)->sin_addr));
    	}
    	WriteFrmtd("%s: %s\n", ifreqs[i].ifr_name,
                inet_ntoa(((struct sockaddr_in*)&ifreqs[i].ifr_addr)->sin_addr));
    }

    if( eth[0] != 0x00 ) {
    	buf = eth;
    } else if( wlan[0] != 0x00 ) {
    	buf = wlan;
    }

    WriteFrmtd("Addr: %s\n", buf);

    return buf;
}


void verifyFileExistsOrCreate(char *pFileName)
{
}

void openServerResponseSharedMemory()
{
	WriteFrmtd("openServerResponseSharedMemory() %d\n", 3);

	key_t key;

	//verifyFileExistsOrCreate("shmkeyfile");
	WriteFrmtd("Getting ftok\n");
	key = ftok("./shmkeyfile", 'B');
	if( key == -1 ) {
		WriteFrmtd("ftok Failed: key = -1\n");
		gpconnInfo = NULL;
		return;
	}
	WriteFrmtd("b4 shmget()\n");
	gshmid = shmget(key, sizeof(connect_info), 0644 | IPC_CREAT);	 // rw-r--r--
	WriteFrmtd("gshmid = %d\n", gshmid);

	WriteFrmtd("Calling shmat()\n");
	gpconnInfo = (pconnect_info)shmat(gshmid, (void *)0, SHM_RDONLY);
	if((char*)gpconnInfo == (char *)(-1))
	{
		WriteFrmtd("After shmat(). Failed: -1\n");
		gpconnInfo = NULL;
		perror("shmat");
	}
	else {
		WriteFrmtd("After shmat() gpconnInfo returned\n");
	}
}


void initServerResponseSharedMemory()
{
	openServerResponseSharedMemory();
	if( gpconnInfo ) {
		gpconnInfo->serverIP[0] = 0x00;
	}
}


void parseServerResponse(cJSON obj)
{
	if( gpconnInfo == NULL )
		return;
	cJSON *prc = cJSON_GetObjectItem(&obj, "server");
	if( prc->type == cJSON_String )
	{
		strcpy(gpconnInfo->serverIP, prc->valuestring);
	}
	prc = cJSON_GetObjectItem(&obj, "port");
	if( prc->type == cJSON_Number ) {
		gpconnInfo->serverPort = prc->valueint;
	}
}

int detachServerResponseSharedMemory()
{
	if( gpconnInfo == NULL )
		return -1;
	shmdt(gpconnInfo);
	return 0;
}

void delServerResponseSharedMemory()
{
	if( gshmid == -1 )
		return;
	struct shmid_ds  shmid;
	shmctl(	gshmid, IPC_RMID, &shmid );
}


int getConnectedServerInfo()
{
	WriteFrmtd("getConnectedServerInfo: %d\n", 2);

	openServerResponseSharedMemory();
	if( gpconnInfo != NULL )
	{
		WriteFrmtd("Back in getConnectedServerInfo() gpconnInfo good!\n");
		//gpconnInfo->serverIP
		//gpconnInfo->serverPort
		if( gpconnInfo->serverPort != 0 ) {
			return 0;
		} else {
			return -1;
		}
	}
	else {
		WriteFrmtd("Back in getConnectedServerInfo() gpconnInfo is NULL!\n");
		return -1;
	}
}


bool fileExists(char *szFile)
{
        if( access( szFile, F_OK) != -1 ) {
                return true;
        } else {
                // Doesn't exist
                return false;
        }
}



