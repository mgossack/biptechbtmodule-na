/*
 * BTLEModule.cpp
 *
 *  Created on: Nov 20, 2014
 *      Author: root
 */

#include "BIPTechInterfaces.h"
#include <BIPTechBTModule/BTSensorTagModule.h>
#include <Poco/Logger.h>
#include <stdlib.h>
#include <BIPTech/BIPSystem.h>
#include <Poco/Thread.h>
#include <Poco/RunnableAdapter.h>

using namespace std;
//using BaseNotification;
using Poco::Thread;

stDataTypeMask btDataTypes[] =
{
	{ "READ", "", "TEMP", "BTLE", 0, "BTLENotification", 0x0000000000000001},
	{ "READ", "", "ACCEL", "BTLE", 0, "BTLENotification", 0x0000000000000002},
	{ "READ", "", "HUMI", "BTLE", 0, "BTLENotification", 0x0000000000000004},
	{ "READ", "", "MAGN", "BTLE", 0, "BTLENotification", 0x0000000000000008},
	{ "READ", "", "BARO", "BTLE", 0, "BTLENotification", 0x000000000000010},
	{ "READ", "", "GYRO", "BTLE", 0, "BTLENotification", 0x0000000000000020},
	{ "READ", "", "KEYS", "BTLE", 0, "BTLENotification", 0x0000000000000040},
	{ "WRITE", "", "KEYS", "BTLE", 0, "BTLENotification", 0x0000000000000080},
	{ "READ", "", "RSSI", "BTLE", 0, "BTLENotification", 0x0000000000000100},
	{ "READ", "", "BATT", "BTLE", 0, "BTLENotification", 0x0000000000000200}
};

namespace BIPTechBluetooth {

BTSensorTagModule::BTSensorTagModule()
{
	int i;
	for(i=0;i<sizeof(btDataTypes); i++)
	{
		_lstDataTypeMask.push_back(&(btDataTypes[i]));
	}

	pRunnable = new Poco::RunnableAdapter<BTSensorTagModule>(*this, &BTSensorTagModule::run);

	_notificationFactory.registerNotification("BTLENotification", &BaseNotification::Create );
}

BTSensorTagModule::~BTSensorTagModule()
{
}


std::list<pDataTypeMask>::iterator BTSensorTagModule::getDataTypeMaskBeginItor()
{
	return _lstDataTypeMask.begin();
}

std::list<pDataTypeMask>::iterator BTSensorTagModule::getDataTypeMaskEndItor()
{
	return _lstDataTypeMask.end();
}

void BTSensorTagModule::start()
{
	_thread.start(*pRunnable);
}

void BTSensorTagModule::stop()
{

}

void BTSensorTagModule::pause()
{

}

void BTSensorTagModule::run()
{
	BIPTechNotification	*pNotification;

	while( _thread.isRunning() ) {
		if( this->usesPriorityQueue() ) {
			pNotification = (BIPTechNotification*)this->getPriorityNotifyQueue()->waitDequeueNotification();
		} else {
			pNotification = (BIPTechNotification*)this->getNotifyQueue()->waitDequeueNotification();
		}

		if(pNotification) {
			// Process the command
			// pNotification->clientCmd()...
		}
	}
}

void BTSensorTagModule::Notify(BIPTechNotification *pNotification)
{
	if( this->usesPriorityQueue() ) {
		this->NotifyWithPriority(pNotification, 10);
	} else {
		this->getNotifyQueue()->enqueueNotification(pNotification);
	}
}

void BTSensorTagModule::NotifyWithPriority(BIPTechNotification *pNotification, int nPriority)
{
	if( this->usesPriorityQueue() ) {
		this->getPriorityNotifyQueue()->enqueueNotification(pNotification, nPriority);
	} else {
		this->getNotifyQueue()->enqueueNotification(pNotification);
	}
}

IBIPTechNotificationFactory* BTSensorTagModule::getModuleNotificationFactory()
{
	return &_notificationFactory;
}

} // namespace BIPTechBluetooth

