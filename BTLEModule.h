/*
 * BTLEModule.h
 *
 *  Created on: Nov 20, 2014
 *      Author: root
 */

#ifndef BTLEMODULE_H_
#define BTLEMODULE_H_

#include <BIPTechBTModule/BIPTechBluetoothModule.h>
#include <BIPTech/DynamicModule.h>
#include <BIPTechInterfaces.h>
#include "Poco/Notification.h"
#include "Poco/NotificationQueue.h"
#include "Poco/PriorityNotificationQueue.h"
#include "Poco/Observer.h"
#include "Poco/NObserver.h"
#include "Poco/AutoPtr.h"
#include "Poco/Thread.h"
#include "Poco/RunnableAdapter.h"
#include <iostream>
#include <map>
#include <list>
#include "cmdstructs.h"
#include "BIPTechInterfaces.h"
#include "BIPTechNotification.h"

using Poco::NotificationCenter;
using Poco::Notification;
using Poco::NotificationQueue;
using Poco::PriorityNotificationQueue;
using Poco::Observer;
using Poco::NObserver;
using Poco::AutoPtr;
using Poco::Thread;
using Poco::RunnableAdapter;

using BIPTechBluetooth::BIPTechNotification;
using BIPTechBluetooth::BaseNotification;
using BIPTechBluetooth::SubNotification;


namespace BIPTechBluetooth {

class BTLETarget
{
public:
	void handleBase(BIPTechBluetooth::BaseNotification* pNf)
	{
//		std::cout << "handleBase: " << pNf->clientCmd()->szCmd << std::endl;
		pNf->release(); // we got ownership, so we must release
	}
	void handleSub(const AutoPtr<BIPTechBluetooth::SubNotification>& pNf)
	{
//		std::cout << "handleSub: " << pNf->clientCmd()->szCmd << std::endl;
	}
};



class BTLEModule: public IBIPTechModule 	/* public DynamicModule */
{
private:
	bool	_scan;		// true or false
	int		_hci;		// HCI #

	std::list<pDataTypeMask>	_lstDataTypeMask;

	Poco::PriorityNotificationQueue		_notifyQueue;
	BluetoothNotificationFactory		_notificationFactory;

private:

	setScanFunc _setScan;
	setHCIFunc	_setHCI;

	Poco::Thread _thread;
	Poco::RunnableAdapter<BTLEModule> *pRunnable;
	void run();

	BTLEModule();

public:
	BTLEModule(std::string path);
	virtual ~BTLEModule();

	bool getScan() { return _scan; }
	int  getHCI() { return _hci; }

	void setScan(bool scan) { _scan = scan; _setScan(_scan); }
	void setHCI(int hci) { _hci = hci; _setHCI(_hci); }

	// IBIPTechModule
	std::list<pDataTypeMask>::iterator getDataTypeMaskBeginItor();
	std::list<pDataTypeMask>::iterator getDataTypeMaskEndItor();
	void start();
	void stop();
	void pause();

	bool usesPriorityQueue() { return true; }
	Poco::NotificationQueue* getNotifyQueue() { return NULL; }
	Poco::PriorityNotificationQueue* getPriorityNotifyQueue() { return &_notifyQueue; }

	void Notify(BIPTechNotification *notification);
	void NotifyWithPriority(BIPTechNotification *notification, int nPriority);

	const IBIPTechNotificationFactory* getModuleNotificationFactory();
};

} // namespace BIPTechBluetooth

#endif /* BTLEMODULE_H_ */
